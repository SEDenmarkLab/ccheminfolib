import os
import sys
from copy import deepcopy
import time
import csv
import operator
import math
##science is fun
from random import shuffle
import numpy as np
import scipy.stats as stats
from scipy.special import inv_boxcox
##lets do statistics
# data preprocessing
from sklearn.preprocessing import StandardScaler, RobustScaler, MaxAbsScaler, MinMaxScaler, Binarizer, Normalizer, PolynomialFeatures
# dimensionality reduction
from sklearn.decomposition import PCA
from sklearn.cross_decomposition import PLSRegression
from sklearn.neighbors import KNeighborsRegressor
#feature selection
from sklearn.feature_selection import SelectKBest, SelectPercentile, chi2, mutual_info_regression, f_regression, VarianceThreshold
#modeling
from sklearn import linear_model, tree, kernel_ridge, cross_decomposition
from sklearn.model_selection import cross_val_score, KFold, train_test_split
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import RandomizedSearchCV
from sklearn.svm import SVR
from sklearn.svm import NuSVR
from sklearn.ensemble import RandomForestRegressor
from sklearn.feature_selection import RFECV
from sklearn.metrics import mean_absolute_error
from sklearn.pipeline import make_pipeline
from sklearn.kernel_ridge import KernelRidge
from pyearth import Earth
#maybe pretty pictures
import matplotlib.pyplot as plt
import matplotlib.cm as cm
	
def kfold_q2(mod, X, y,splits=10):
	X = np.array(X)
	y = np.array(y)
	
	#get splits
	kf = KFold(n_splits=splits)
	Q2_scores = []
	sum_PRESS = 0.0
	sum_TSS = 0.0
	for train_index, test_index in kf.split(X):
		X_train, X_test = X[train_index], X[test_index]
		y_train, y_test = y[train_index], y[test_index]
		box_shift = abs(min(y_train+y_test))+1.0
		# scale the training set
		#training_set_scaler = MaxAbsScaler()
		#X_train = training_set_scaler.fit_transform(X_train)
		y_train, lambda_ = stats.boxcox(y_train+box_shift)
		
		mod.fit(X_train, y_train)
		#y_hat_train = mod.predict(X_train)
		# scale the test sets
		#X_test = training_set_scaler.transform(X_test)
		y_hat_box = mod.predict(X_test)
		y_hat_test = inv_boxcox(y_hat_box, lambda_) - box_shift
		# calculuate PRESS_ext
		sum_pred_error = 0.0
		
		for x in range(len(y_test)):
			sum_pred_error += (y_hat_test[x] - y_test[x]) ** 2
		PRESS = sum_pred_error / float(len(y_test))
		
		# calculate TSS_TS
		sum_total_sum_squares = 0.0
		for x in range(len(y_train)):
			sum_total_sum_squares += (y_train[x] - np.mean(y_train)) ** 2
		TSS = sum_total_sum_squares / float(len(y_train))
		sum_PRESS += PRESS
		sum_TSS += TSS
		Q2 = 1.0 - (PRESS/TSS)
		Q2_scores.append(Q2)
	return Q2_scores, 1.0 - (sum_PRESS/sum_TSS)
## make models takes the Y_DATA, a Y_transform, which in this example is only boxcox but could be any Y-transform, the X_transform, 
##which specifies feature preprocessing, and a label which is used in naming the output
def make_models(Y_DATA, Y_TRANSFORM, X_TRANSFORM, label):
		X_DATA = X_TRANSFORM.fit_transform(X, y_boxcox)
		try:
			sp_model_dir = model_dir + label + '/'
			os.mkdir(sp_model_dir)
		except:
			pass
		output_file = open(sp_model_dir+'model_output.csv','w')
		output_file.write('Model Info\n')
		output_file.write('X,'+str(X_TRANSFORM)+'\n')
		output_file.write(str(Y_TRANSFORM))
		## lets make some models
		# note: adjust desired models to be trained by updating the models dictionary here
		models = {}
		models['SVR_POLY2_G'] = GridSearchCV(SVR(kernel='poly', degree=2), cv=5, n_jobs=8, param_grid={"C": [0.001, 0.01, 0.1, 1, 10, 100, 1000, 10000], "gamma": [0.00001, 0.0001, 0.001, 0.01, 0.1, 1, 10, 100, 1000], "epsilon": [0.01, 0.1, 0.5, 1, 2, 4]})
		models['SVR_RBF'] = GridSearchCV(SVR(kernel='poly'), cv=5, n_jobs=8, param_grid={"C": [0.001, 0.01, 0.1, 1, 10, 100, 1000, 10000], "gamma": [0.00001, 0.0001, 0.001, 0.01, 0.1, 1, 10, 100, 1000], "epsilon": [0.01, 0.1, 0.5, 1, 2, 4]})
		models['SVR_linear'] = GridSearchCV(SVR(kernel='linear'), cv=5, n_jobs=8, param_grid={"C": [0.001, 0.01, 0.1, 1, 10, 100, 1000, 10000], "epsilon": [0.01, 0.1, 0.5, 1, 2, 4]})
		models['LASSO'] = linear_model.LassoCV(max_iter=100000, cv=5, n_jobs=4)
		models['LASSOLARS'] = linear_model.LassoLarsCV(max_iter=5000, cv=5, n_jobs=4)
		models['RIDGE'] = linear_model.RidgeCV(cv=5)
		models['KR_RBF'] =GridSearchCV(KernelRidge(kernel='rbf'), cv=5, n_jobs=4, param_grid={"alpha": [10, 1, 0.1, 1e-2, 1e-3], "gamma": [0.00001, 0.0001, 0.001, 0.01, 0.1, 1, 10, 100, 1000]})
		models['KR_linear'] =GridSearchCV(KernelRidge(kernel='linear'), cv=5, n_jobs=4, param_grid={"alpha": [10, 1, 0.1, 1e-2, 1e-3]})
		models['EN'] = linear_model.ElasticNetCV(max_iter=100000,cv=5, n_jobs=4)
		models['RF'] = GridSearchCV(RandomForestRegressor(n_estimators=10000, n_jobs=4), cv=5, n_jobs=4, param_grid={'max_features': [0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9]})
		models['PLS8'] = cross_decomposition.PLSRegression(n_components=8, scale=False)
		models['PLS10'] = cross_decomposition.PLSRegression(n_components=10, scale=False)
		models['MARS'] = Earth()
		# q2 variables
		q2 = {}
		r2 = {}
		q2_shuffle = {}
		y_shuffle = deepcopy(Y_DATA)
		shuffle(y_shuffle)
		y_shuffle = np.array(y_shuffle)
		# get X and y lists from the current training set
		
		# train the models on the training set data 
		output_file.write('MODEL,Q2,Q2 Shuffle, R2\n') 
		for model in models:
			print model
			# get cv scores
			scores = cross_val_score(models[model], X_DATA, Y_DATA, scoring='r2', cv=5)
			shuffle_scores = cross_val_score(models[model], X_DATA, y_shuffle, scoring='r2', cv=5)
			
			models[model].fit(X_DATA, Y_DATA)
			if Y_TRANSFORM == 'boxcox':
				y_hat = inv_boxcox(models[model].predict(X_DATA).ravel(), lambda_) - abs(y_min) - 1
			else:
				y_hat = models[model].predict(X_DATA).ravel()
			
			q2[model] = scores.mean()
			r2[model] = models[model].score(X_DATA, Y_DATA)
			
			q2_shuffle[model] = shuffle_scores.mean()
			models[model].fit(X_DATA, Y_DATA)
			
			with open(sp_model_dir+model+'.csv', 'wb') as csvfile:
				csv_writer = csv.writer(csvfile, delimiter=',')
				csv_writer.writerow([str(models[model])])
				csv_writer.writerow(['PRED','OBS','RESIDUALS'])
				for x in range(len(y_hat)):
					csv_writer.writerow([y_hat[x],y[x], (y[x]-y_hat[x])])
			lab = []
			Y_lib_hat = []
			X_l = []
			for label in X_lib:
				lab.append(label)
				X_l.append(X_lib[label])
			# Transform X data, predict selectivity
			X_l = X_TRANSFORM.transform(X_l)
			if Y_TRANSFORM == 'boxcox':
				Y_lab_hat = inv_boxcox(models[model].predict(X_l).ravel(), lambda_) - abs(y_min) - 1
			else:
				Y_lab_hat = models[model].predict(X_l).ravel()
			
			nf = open(sp_model_dir +'_'+ model + '_predicted_values.csv', 'w')
			for x in range(len(X_l)):
				nf.write(str(lab[x]) + ',' + str(Y_lab_hat[x]) + '\n')
			
			# write out the model metrics for the current iteration	
			#output_file.write('METRICS FOR ITERATION' + str(iter_count)+'\n')

			output_file.write(model + ',' + str(q2[model]) + ',' + str(q2_shuffle[model]) + ',' + str(r2[model]) + '\n')
			

if __name__ == '__main__':
	# lets make sure our models are always comparable -- set random state
	seed = 7
	np.random.seed(7)
	
	# directories
	descriptor_dir = #path do descriptor dir
	observable_dir = #path to selectivity data
	output_dir 	   = #designated output file dir
	model_dir = output_dir + 'models_' + time.strftime("%Y%m%d-%H%M%S")+'/' #output file directories are named by time to help differentiate different modeling attempts
	try:
		os.mkdir(model_dir)
	except IOError:
		print "Could not make directory for output!"
	## get observable data
	# same system with the observable data...one dict for determining observable catalyst relationship
	# and one for transforming the y data
	y_lib = {}
	y_labels = []
	y_total = []
	with open(observable_dir+'ER_DATA_extrap_train.csv','rb') as csvfile:
		csv_reader = csv.reader(csvfile, delimiter=',')
		for row in csv_reader:
			y_labels.append(row[0])
			y_total.append(float(row[6]))
	## get descriptor data
	# the dictionary will hold the features in lists with the catalyst label as the keys {'1_1_1_1': [features]}
	X_lib = {}
	# this list is for fitting the data transform we use for X, should we use one
	X_total = []
	X_label = []
	with open(descriptor_dir+'combined_descriptors2.csv','rb') as csvfile:
		csv_reader = csv.reader(csvfile, delimiter=',')
		for row in csv_reader:
			X_lib[row[0]] = [float(x) for x in row[1:]]
			X_total.append(X_lib[row[0]])
			X_label.append(row[0])
	X = []
	for cat in y_labels:
		X.append(X_lib[cat])
		y = np.asarray(y_total)
		y_min = min(y)
		y_s = y + abs(y_min) + 1
		y_boxcox, lambda_ = stats.boxcox(y_s)
	#making all the necessary X data
	
	X_persel_mir_25_boxcox_t = SelectPercentile(mutual_info_regression, percentile=25)
	make_models(y_boxcox, 'boxcox', X_persel_mir_25_boxcox_t, 'X_persel_mir_25_boxcox')
	
	X_persel_freg_25_boxcox_t = SelectPercentile(f_regression, percentile=25)
	make_models(y_boxcox, 'boxcox', X_persel_freg_25_boxcox_t, 'X_persel_freg_25_boxcox')
			
	X_PCA_30_boxcox_t = PCA(n_components=30)
	make_models(y_boxcox, 'boxcox', X_PCA_30_boxcox_t, 'X_PCA_30 y_boxcox')
	
	X_PCA_2000_boxcox_t = PCA(n_components=2000)
	make_models(y_boxcox, 'boxcox', X_PCA_2000_boxcox_t, 'X_PCA_2000 y_boxcox')	
		
	X_select_all_boxcox_t = SelectKBest(f_regression, k='all')
	make_models(y_boxcox, 'boxcox', X_select_all_boxcox_t, '(X_select_all_boxcox')