import os
import sys
import cPickle as pickle
from copy import deepcopy
from multiprocessing import Pool
# ccheminfolib imports
# cchemlib
# datatypes
from ccheminfolib.cchemlib import datatypes as dt
from ccheminfolib.cchemlib import atomtypes as dt
from ccheminfolib.cchemlib.datatypes import Molecule
from ccheminfolib.cchemlib import bondtypes as bt
# parsers
from ccheminfolib.cchemlib import mol2Parser
# cdesclib
# calculators
from ccheminfolib.cdesclib import AverageOccupancyCalculator

# This is an example script about how to use the Average Occupancy Calculator in ccheminfolib to calculate occupancy descriptors

mol2_dir = '' # designate directory of aligned conformer files. Files should have a systematic nomenclature designating a catalyst handle (cat) and conformer number (n)
# Nomeclature example for two catalyst with 2 conformers each: cat-a_1, cat-a_2, cat-b_1, cat-b_2
occ_dir = '' # designate directory of output files that will contain individual aso files for each catalyst
def process_calculator(file_list):
	grid_file = open('YOUR_GRID_HERE.grid','rb') # grid file from calculate_aligned_grids.py
	grid = pickle.load(grid_file)
	grid_file.close()
	
	mols = []
	core = file_list[0].rsplit('_', 1)[0] # identify catalyst core - depending on your nomenclature system this may need to be modified
	print "Loading conformers for " + core
	for file in file_list:
		p = mol2Parser(mol2_dir + file + '.mol2', file) # parse mol2 files to construct mol object
		p.parse() 
		mol = p.molecule()
		if isinstance(mol, Molecule): # put molecule in grid
			mol.grid = deepcopy(grid)
			mols.append(mol)
		else:
			print "ERROR PROCESSING MOLECULE"
			continue
	print "Calculating occupancy..."
	occ = AverageOccupancyCalculator(mols, grid) # Use of the average occupancy calculator in ccheminfolib
	new_grid = deepcopy(occ.calculate()) # Use of the average occupancy calculator in ccheminfolib
	#new_grid = occ.average_occupancy_grid
	print "Length of new grid... " + str(len(new_grid.keys()))
	wf = open(occ_dir+core+'.csv','w') # the ASO file for each molecule is saved in occ_dir (specified above) and saved as the core.csv
	wf.write(core+',')
	for gridpoint_ID in range(len(new_grid.keys())):
		# because dt.OCC won't work for some reason, its 5 
		wf.write(str(new_grid[gridpoint_ID].descriptors[5])+',')
	wf.write('\n')
	wf.close()
	del mols
	del occ
	del new_grid
	del grid
	print "Processed core: " + core

if __name__ == '__main__':
	
	# load the grid
	grid_file = open('product.grid','rb')  # grid file from calculate_aligned_grids.py
	grid = pickle.load(grid_file)
	grid_file.close()
	print "Grid contains... " + str(len(grid.keys())) + " points!" # size of grid
	
	# open the descriptor file
	#wf = open('occupancy.csv','w')
	#get already done ones
	already_done = []
	for root,dir,files in os.walk(occ_dir): # specify occ_dir above - this checks for files that are already complete to avoid duplicates
		for file in files:
			if '.csv' in file:
				already_done.append(file.split('.')[0])
	## now, we iterate through the molecule types, 
	## generating conformer lists
	
	core_labels = []
	conformers = {}
	conformer_count = 0
	for root,dir,files in os.walk(mol2_dir):
		for file in files:
			name = file.split('.')[0].rsplit('_', 1)[0]
			if name in already_done:
				print "Already did: " + name
				continue
			else:
				pass
			if name not in core_labels:
				core_labels.append(name)
				conformers[name] = []
				conformers[name].append(file.split('.')[0])
				conformer_count += 1
			else:
				conformers[name].append(file.split('.')[0])
				conformer_count += 1
	print "Found a total of: " + str(len(core_labels)) + " core molecules and " + str(conformer_count) + " molecules!"
	
	# now make a master list of lists
	conformer_list = [conformers[core] for core in conformers.keys()]
	
	pool = Pool(processes=2)
	pool.map(process_calculator, conformer_list)
	
	