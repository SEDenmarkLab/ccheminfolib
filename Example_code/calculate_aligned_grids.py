import os
import sys
from copy import deepcopy
import cPickle as pickle
#datatypes 
from ccheminfolib.cchemlib import atomtypes as at
from ccheminfolib.cchemlib import bondtypes as bt
from ccheminfolib.cchemlib import datatypes as dt
#parsers
from ccheminfolib.cchemlib import mol2Parser
#construction
from ccheminfolib.cdesclib import GridConstructor

if __name__ == '__main__':
	
	#get the mols
	mol2_dir = '' # Specify the directory that contains the separated, aligned conformers of all molecules in the in silico library.
				  # The nomenclature of the files should contain a unique catalyst handle and a conformer number (e.g. cat-a_1,cat-a_2,cat-b_1... etc)
	mol_dir = ''  # Specify mol_dir (grid file will be saved here)
	mols = []
	
	for root, dir, files in os.walk(mol2_dir):
		for file in files:
			if '.mol2' not in file:
				continue
			p = mol2Parser(mol2_dir+file, file.split('.')[0])
			p.parse()
			mol = p.molecule()
			mols.append(deepcopy(mol))
			print 'Loaded: ' + file
			del p
			del mol
	constructor = GridConstructor(mols, spacing=1.0, homogenize=False)
	grid = constructor.generate_grid()
	f = open(mol_dir+'product.grid','wb')
	pickle.dump(grid, f)
	f.close()
